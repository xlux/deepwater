import os
import numpy as np
# keras = tf.compat.v1.keras
# Sequence = keras.utils.Sequence
import tensorflow as tf
import tensorflow.compat.v1.keras as keras
import cv2
from .utils import Normalizer, load_flist, safe_quantization, find_sequences, get_flist_dic, get_image_id, unfold_3D
from .config import Config
from .preprocessing import get_pixel_weight_function, preprocess_gt, smooth_components
from datetime import datetime
import random
import tifffile as tiff


from .eidg import random_transform
from .elastic_transform import elastic_transform


class SampleGenerator(tf.keras.utils.Sequence):
    """Generates data for Keras"""
    def __init__(self,
                 img_flist,
                 gt_flist,
                 config,
                 mask_flist=None,
                 augmentation=True,
                 markers=False):

        """Initialization"""
        self.batch_size = config.BATCH_SIZE
        self.markers = markers
        self.indexes = np.arange(len(img_flist))
        self.random_state = np.random.RandomState(config.SEED)
        self.norm = Normalizer(config.NORMALIZATION, config.UNEVEN_ILLUMINATION)
        self.dim = config.DIM
        self.digits = config.DIGITS

        # self.dim_original = config.DIM_ORIGINAL
        self.shuffle = config.MODE == 1
        self.shrink = config.SHRINK
        self.marker_diameter = config.MARKER_DIAMETER
        self.weights = config.WEIGHTS
        self.gt_depth = 2  # - (config.PIXEL_WEIGHTS == 'NO_WEIGHTS')*1
        self.weight_function = get_pixel_weight_function(config.PIXEL_WEIGHTS)
        self.img_flist = img_flist
        self.gt_flist = gt_flist
        self.mask_flist = mask_flist
        self.border_crop = config.BORDER_MASK

        self.input_depth = config.INPUT_DEPTH

        self.debug = config.DEBUG
        self.debug_dir = config.DEBUG_DIR
        self.augmentation = augmentation
        self.rotation_range = 180
        self.zoom_range = 0.3
        self.fill_mode = 'reflect'
        self.flip = True
        self.elastic_transform = False #config.ELASTIC_TRANSFORM
        self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch"""
        n_batches = int(np.ceil(len(self.img_flist) / self.batch_size))
        return n_batches

    def __getitem__(self, index):
        """Generate one batch of data"""
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        seed = random.randint(0, np.iinfo(np.uint32).max)
        m, n = self.dim

        x = np.zeros((len(indexes), m, n, self.input_depth), dtype=np.float32)
        y = np.zeros((len(indexes), m, n, 1), dtype=np.float32)
        w = np.zeros((len(indexes), m, n, 1), dtype=np.float32)

        for i, index in enumerate(indexes):
            # TODO: load images also in other formats (color, 16 bits)
            #       returns images in the format self.dim
            #       and also normalized and centered in between -.5 and .5
            #       img has depth self.input_depth
            img = self.get_image(index)
            gt, mask = self.get_gt(index)
            assert len(img.shape) == 3, f'Shapes fo one sample: {img.shape}, {self.dim}'

            if self.augmentation:
                # expected ordering: widht, height, channels

                stack = np.concatenate((img, gt, mask), axis=2)
                w_dim, h_dim, d = stack.shape
                assert d == 2 + self.input_depth
                assert (w_dim, h_dim) == self.dim
                stack = random_transform(stack,
                                         dim_ordering='tf',
                                         rotation_range=self.rotation_range,
                                         zoom_range=self.zoom_range,
                                         fill_mode=self.fill_mode,
                                         horizontal_flip=self.flip,
                                         elastic_transform=self.elastic_transform,
                                         sync_seed=seed)
                assert stack.shape == (w_dim, h_dim, d)
                img, gt, mask = np.split(stack, [self.input_depth, self.input_depth+1], axis=2)

            x[i, :, :, :] = img
            y[i, :, :, :] = gt
            w[i, :, :, :] = mask

        assert len(x.shape) == 4
        assert len(y.shape) == 4
        assert len(w.shape) == 4

        # if self.elastic_transform and False:
        #     sample = np.stack((x, y, w), axis=3)
        #     h = x.shape[1]
        #     for i in range(sample.shape[0]):
        #         sample[i, :, :, :] = elastic_transform(sample[i, :, :, :], h * 0.5, h * 0.04, None)
        #     x, y, w = np.split(sample, 3, axis=3)
        #     x = np.squeeze(x)
        #     y = np.squeeze(y)
        #     w = np.squeeze(w)
        #
        # y = (y > 0) * 1
        # w = (w > 0) * 1

        if self.debug:
            img_index = self.input_depth // 2
            for i, index in enumerate(indexes):
                gt = (y[i, :, :, 0] * 255).astype(np.uint8)
                img = ((x[i, :, :, img_index] + 0.5) * 255).astype(np.uint8)
                weight = (w[i, :, :, 0] * 255).astype(np.uint8)
                cv2.imwrite(os.path.join(self.debug_dir, f'debug_{i}_{index}_{seed}_aug_gt.png'), gt)
                cv2.imwrite(os.path.join(self.debug_dir, f'debug_{i}_{index}_{seed}_aug_img.png'), img)
                cv2.imwrite(os.path.join(self.debug_dir, f'debug_{i}_{index}_{seed}_aug_w.png'), weight)
                composit = np.stack([weight, img, gt], axis=2)
                assert cv2.imwrite(os.path.join(self.debug_dir, f'debug_{i}_{index}_{seed}_aug_comp.png'), composit), \
                    composit.shape
        # Find list of IDs
        if self.debug > 2:
            """ store x and y to check quality """
            now = datetime.now().strftime("%H%M%S")
            np.save(f'x_{index}_{now}', x)
            np.save(f'y_{index}_{now}', y)

        y = np.concatenate((y, w), axis=3)
        assert len(x.shape) == 4
        assert len(y.shape) == 4
        return x, y

    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        if self.shuffle:
            # np.random.seed(self.seed)
            np.random.shuffle(self.indexes, )

    def get_image(self, index):
        w, h = self.dim
        stack = np.zeros((w, h, self.input_depth))
        paths = self.img_flist[index]

        assert len(paths) == self.input_depth, f'{paths}, {len(paths)}, {self.input_depth}'

        for i, path in enumerate(paths):
            img = cv2.imread(path, cv2.IMREAD_ANYDEPTH)
            img = (self.norm.make(img) - .5)
            assert self.dim != (0, 0), path
            img = pad_zeros(img, self.dim)
            stack[:, :, i] = img

        assert len(stack.shape) == 3
        return stack

    def get_gt(self, index):
        # retruns both gt and mask
        # both has shape (m, n, 1) = (self.dim, 1)

        # check index validity
        assert len(self.gt_flist) > index,\
            f'{self.gt_flist}, {len(self.gt_flist)}, {index}'

        m, n = self.dim

        gt_path = self.gt_flist[index]
        gt = cv2.imread(gt_path, cv2.IMREAD_ANYDEPTH)
        gt = preprocess_gt(gt, shrink=self.shrink, markers=self.markers)
        gt = safe_quantization(gt, dtype=np.uint8)

        mask_path = self._get_mask_path(gt_path)
        if os.path.isfile(mask_path):
            mask = cv2.imread(mask_path, cv2.IMREAD_ANYDEPTH) > 0
        else:
            m_gt, n_gt = gt.shape
            bc = self.border_crop
            shape = max(m_gt - 2 * bc, 0), max(n_gt - 2 * bc, 0)
            mask = np.zeros((m_gt, n_gt))
            mask[bc:-bc, bc:-bc] = np.ones(shape)

        gt = pad_zeros(gt, self.dim)
        mask = pad_zeros(mask, self.dim)

        return gt.reshape((m, n, 1)), mask.reshape((m, n, 1))

    def _get_mask_path(self, gt_path):
        path, name = os.path.split(gt_path)
        gt_id = get_image_id(gt_path)
        mask_name = f'mask{gt_id}.tif'
        return os.path.join(path, mask_name)

    def get_all(self):
        batches_x = []
        batches_y = []
        for i in range(len(self)):
            x, y = self[i]
            batches_x.append(x)
            batches_y.append(y)
        x = np.concatenate(batches_x, axis=0)
        y = np.concatenate(batches_y, axis=0)
        return x, y


# TODO: update test sample generatro
class TestSampleGenerator(keras.utils.Sequence):
    """Generates testing data for Keras"""
    def __init__(self,
                 img_flist,
                 config):

        """Initialization"""
        self.batch_size = config.BATCH_SIZE
        self.img_flist = img_flist
        self.indexes = np.arange(len(self.img_flist))
        self.seed = config.SEED
        self.norm = Normalizer(config.NORMALIZATION, config.UNEVEN_ILLUMINATION)
        self.dim = config.DIM
        self.dim_original = config.DIM_ORIGINAL
        self.shuffle = config.MODE == 1
        self.shrink = config.SHRINK
        self.debug = config.DEBUG
        self.input_depth = config.INPUT_DEPTH

        self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch"""
        length = np.ceil(len(self.img_flist) / self.batch_size)
        if self.debug:
            print(f'dataset length: {length}')
        return length.astype(np.int)

    def __getitem__(self, index):
        """Generate one batch of data"""
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        indexes = [i for i in indexes if i < len(self.img_flist)]

        m, n = self.dim
        x = np.zeros((len(indexes), m, n, self.input_depth), dtype=np.float32)

        for i, index in enumerate(indexes):
            img = self.get_image(index)
            x[i, :, :, :] = img
        if self.debug:
            print(x.shape, indexes)

        return x

    def get_all(self):
        batches = []
        for i in range(len(self)):
            batches.append(self[i])
        x = np.concatenate(batches, axis=0)
        return x

    def _read_image(self, index):
        img = cv2.imread(self.img_flist[index], cv2.IMREAD_ANYDEPTH)
        if len(img.shape) == 2:
            img = np.expand_dims(img, axis=2)
        assert len(img.shape) == 3
        return img

    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        if self.shuffle:
            # np.random.seed(self.seed)
            np.random.shuffle(self.indexes, )

    def get_image(self, index):
        w, h = self.dim
        stack = np.zeros((w, h, self.input_depth))
        paths = self.img_flist[index]

        assert len(paths) == self.input_depth, f'{paths}, {len(paths)}, {self.input_depth}'

        for i, path in enumerate(paths):
            img = cv2.imread(path, cv2.IMREAD_ANYDEPTH)
            img = (self.norm.make(img) - .5)
            img = pad_zeros(img, self.dim)
            stack[:, :, i] = img

        assert len(stack.shape) == 3
        return stack


def pad_zeros(img, dim):
    result = np.zeros(dim)
    mo, no = img.shape
    result[:mo, :no] = img
    assert len(result.shape) == 2
    return result


class Dataset:
    """
    class to provide data in a proper format
    maintain data generator given by the config file
    dataset follows the structure of CTC datasets
    """
    def __init__(self,
                 config: Config = None,
                 markers: bool = False):
        self.config = config
        self.markers = markers
        self.mode = config.MODE
        self.name = config.DATASET_NAME
        self.out_paths = config.OUT_PATH
        self.seq = config.SEQUENCE
        self.seed = config.SEED
        self.batch_size = config.BATCH_SIZE
        self.pixel_weights = config.PIXEL_WEIGHTS
        self.dim = config.DIM
        self.marker_masks = config.MARKER_MASKS
        self.cell_masks = config.CELL_MASKS
        self.reference = config.REFERENCE
        self.digits = config.DIGITS
        self.data_path = config.DATA_PATH
        self.validation_sequence = config.VALIDATION_SEQUENCE
        self.generator = None
        self.seq_length = config.SEQ_LENGTH
        self.input_suffix = config.INPUT_SUFFIX
        self.input_depth = config.INPUT_DEPTH

        #  get flists
        self.img_sequences = self._get_source_seq()
        self.flist_img = None
        self.flist_gt = None
        self.flist_mask = None
        self.flist_img_val = None
        self.flist_gt_val = None
        self.flist_mask_val = None

        self.update_flists()

    def __len__(self):
        return len(self.flist_img)

    def get_validation_samples_generator(self):
        return SampleGenerator(img_flist=self.flist_img_val,
                               gt_flist=self.flist_gt_val,
                               mask_flist=self.flist_mask_val,
                               config=self.config,
                               augmentation=False,
                               markers=self.markers)

    def get_training_samples_generator(self):
        return SampleGenerator(img_flist=self.flist_img,
                               gt_flist=self.flist_gt,
                               mask_flist=self.flist_mask_val,
                               config=self.config,
                               augmentation=True,
                               markers=self.markers)

    def get_img_generator(self):
        return TestSampleGenerator(img_flist=self.flist_img,
                                   config=self.config)

    def get_batch(self, index):
        if self.generator in None:
            self.generator = self.get_img_generator()
        return self.generator[index]

    def _get_flists(self,
                    sequences=None,
                    shuffle=True):

        gt_flist = []
        img_flist = []
        mask_flist = []

        if sequences is None:
            sequences = self.img_sequences

        for seq in sequences:

            img_source_name = seq
            if self.input_suffix:
                img_source_name = f'{seq}_{self.input_suffix}'

            img_path = os.path.join(self.config.DATA_PATH, self.name, img_source_name)
            if not os.path.isdir(img_path):
                print(f'the following path do not exist: {img_path}')

                img_3d_path = os.path.join(self.config.DATA_PATH, self.name, seq)
                if self.input_suffix == '2D' and os.path.isdir(img_3d_path):
                    print("unfolding input images")
                    unfold_3D(img_3d_path)
                    assert os.path.isdir(img_path), img_path

            img_files = load_flist(img_path, prefix='t')

            if self.mode in [1, 4]:
                if self.markers:
                    gt_path = os.path.join(self.config.DATA_PATH,
                                           self.name,
                                           f'{seq}_{self.reference}',
                                           self.marker_masks)
                    # gt_path = self.marker_masks
                else:
                    gt_path = os.path.join(self.config.DATA_PATH,
                                           self.name,
                                           f'{seq}_{self.reference}',
                                           self.cell_masks)
                print(self.markers)
                if not os.path.isdir(gt_path):
                    print(f'the following path do not exist: {gt_path}')
                    continue

                img_flist_dict = get_flist_dic(img_files, self.input_depth)
                gts = load_flist(gt_path, prefix='man_')
                masks = load_flist(gt_path, prefix='mask')
                imgs = []
                # print(gt_path)
                # print(gts)

                sample_ids = [get_image_id(gt) for gt in gts]
                for s_id in sample_ids:
                    file_name = img_flist_dict.get(s_id, None)
                    assert file_name, f"there is no image with id {s_id} in {img_path}. {gt_path}"
                    imgs.append(file_name)

                # normalize flist lenght
                if self.seq_length > 0:
                    assert len(imgs) != 0,  f'No imgs found: {seq}, {gts}'
                    assert len(gts) != 0, f'No gts found: {seq}, {gts}'
                    imgs = [imgs[i % len(imgs)] for i in range(self.seq_length)]
                    gts = [gts[i % len(gts)] for i in range(self.seq_length)]
                    # masks = [masks[i % len(masks)] for i in range(self.seq_length)]

                img_flist.extend(imgs)
                gt_flist.extend(gts)
                mask_flist.extend(masks)
                print(f'Add {len(imgs)}/{len(img_flist)}')
            else:
                img_flist_dict = get_flist_dic(img_files, self.input_depth)
                img_flist = sorted(img_flist_dict.values())

        # set self.mask parameter to <True>, if there are masking files in the gt folder
        # self.masks = (mask_flist is not None) and (len(mask_flist) == len(gt_flist)) and self.masks
        # print("Masks:", self.masks)
        assert len(img_flist) == len(gt_flist) or len(gt_flist) == 0, ''
        assert len(img_flist) > 0, f'no samples were found\nIMG path: {img_path}'
        return img_flist, gt_flist, mask_flist

    def _get_source_seq(self):
        if self.mode == 1:
            # list all the possible sequence directories
            # exclude validation_seq

            dirs = find_sequences(os.path.join(self.data_path, self.name))
            if self.validation_sequence is not None:
                dirs.remove(self.validation_sequence)

            assert len(dirs) > 0, f'no training sequences were found in {self.data_path}'
            return dirs

        else:
            return [self.seq]

    def update_flists(self):
        self.flist_img, self.flist_gt, self.flist_mask = self._get_flists()

        if self.mode in [1, 4]:
            if self.validation_sequence is None:
                total = len(self.flist_img)
                assert total >= 2, 'dataset with less than two samples' \
                                   'cannot be split into a training and' \
                                   'a validation part'
                split_index = max(min(total // 10, 50), 1)

                print(f'Validation on the first {split_index} samples, {total} samples in total')

                # shuffle
                np.random.seed(self.seed)
                np.random.shuffle(self.flist_img, )
                np.random.seed(self.seed)
                np.random.shuffle(self.flist_gt, )
                np.random.seed(self.seed)
                np.random.shuffle(self.flist_mask, )

                self.flist_img_val = self.flist_img[:split_index]
                self.flist_gt_val = self.flist_gt[:split_index]
                self.flist_mask_val = self.flist_mask[:split_index]
                self.flist_img = self.flist_img[split_index:]
                self.flist_gt = self.flist_gt[split_index:]
                self.flist_mask = self.flist_mask[split_index:]

                # sort
                self.flist_img_val.sort()
                self.flist_gt_val.sort()
                self.flist_mask_val.sort()
                self.flist_img.sort()
                self.flist_gt.sort()
                self.flist_mask.sort()

                assert len(self.flist_img_val) == len(self.flist_gt_val)
                assert len(self.flist_img) == len(self.flist_gt)

            else:
                print(self.validation_sequence)
                self.flist_img_val, self.flist_gt_val, self.flist_mask_val = \
                    self._get_flists(sequences=self.validation_sequence)

    def get_image(self, index):
        if self.generator is None:
            self.generator = self.get_img_generator()
        return self.generator.get_image(index)

    def save_all(self, name='unknown'):
        if self.generator is None:
            self.generator = self.get_img_generator()
        x = self.get_all()
        img, gt = x
        np.save(f'reference_images_{name}.npy', img)
        np.save(f'reference_gts_{name}.npy', gt)

    def get_all(self):
        generator = self.get_training_samples_generator()
        return generator.get_all()

    def get_original_size(self):
        img_path = self.flist_img[0][0]
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        return img.shape

    def get_z_depth(self):
        sequences = self.img_sequences

        for seq in sequences:
            img_path = os.path.join(self.config.DATA_PATH, self.name, seq)

            assert os.path.isdir(img_path), img_path
            files = os.listdir(img_path)
            assert len(files) > 0
            img_stack = tiff.imread(os.path.join(img_path, files[0]))
            if len(img_stack.shape) == 2:
                return 1
            else:
                return img_stack.shape[0]
