from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, UpSampling2D, Concatenate, Add, BatchNormalization, Activation
from tensorflow.keras.optimizers import Adam, Nadam, SGD
from tensorflow.keras.regularizers import l1_l2
from .dataset import Dataset
import tensorflow as tf
import os
from .loss_functions import get_loss_function
from .utils import get_regularizer, compute_jaccard, write_f_threshold
from tqdm import tqdm

from .callbacks import ReduceLRCallback
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import TensorBoard
from matplotlib import pyplot as plt

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)


class UNetModel():
    """
    instance of model, that predicts cell markers
    """
    def __init__(self, config, markers=False, out_depth=1):
        # super(UNetModel, self).__init__()

        self.config = config

        self.dim = config.DIM
        self.input_depth = config.INPUT_DEPTH
        self.lr = config.LR
        self.feature_maps = config.FEATURE_MAPS
        self.downsampling_levels = config.DOWNSAMPLING_LEVELS
        self.output_layers = out_depth
        self.debug = config.debug
        self.markers = markers
        self.loss_function = get_loss_function(config.LOSS_FUNCTION)
        self.steps_per_epoch = config.STEPS_PER_EPOCH
        self.epochs = config.EPOCHS
        self.topology = config.TOPOLOGY
        self.regularizer = get_regularizer(config.REGULARIZER)
        # self.regularizer = l1_l2
        self.tb_path = os.path.join(config.CONFIG_PATH, config.MODEL_NAME, 'tb_logs')
        tf.random.set_seed(2)

        ## tb bug
        self._get_distribution_strategy = lambda: None
        self.run_eagerly = False

        if self.topology == "ResidualUNet":
            input_layer, output_layer = self._create_resudual_unet()
        else:
            input_layer, output_layer = self._create_unet()
        self.inputs = input_layer
        self.outputs = output_layer

        self.model = Model(inputs=self.inputs, outputs=self.outputs)
        self.model.compile(optimizer=Nadam(lr=self.lr), loss=self.loss_function)
        # self.model.compile(optimizer=Adam(lr=self.lr), loss='mse')
        self.model.summary()

    # def call(self, inputs):
    #     return self.outputs(inputs)

    def _create_level(self, input_layer, level, n_maps):
        if level == 0:
            return input_layer
        else:
            c1 = Conv2D(n_maps,
                        (3, 3),
                        activation='relu',
                        padding='same',
                        kernel_regularizer=self.regularizer)(input_layer)
            pool = MaxPooling2D((2, 2), padding='same')(c1)
            c2 = Conv2D(n_maps*2,
                        (3, 3),
                        activation='relu',
                        padding='same',
                        kernel_regularizer=self.regularizer)(pool)

            down = self._create_level(c2, level - 1, n_maps * 2)

            c3 = Conv2D(n_maps*2,
                        (3, 3),
                        activation='relu',
                        padding='same',
                        kernel_regularizer=self.regularizer)(down)
            upsampling = UpSampling2D((2, 2), interpolation='bilinear')(c3)
            concatenate = Concatenate(axis=3)([upsampling, c1])
            c4 = Conv2D(n_maps,
                        (3, 3),
                        activation='relu',
                        padding='same',
                        kernel_regularizer=self.regularizer)(concatenate)

            return c4

    def _create_unet(self):

        m, n = self.dim
        input_layer = Input(shape=(m, n, self.input_depth))
        n_maps = self.feature_maps
        level = self.downsampling_levels

        c1 = Conv2D(n_maps,
                    (3, 3),
                    activation='relu',
                    padding='same',
                    kernel_regularizer=self.regularizer)(input_layer)
        down = self._create_level(c1, level, n_maps)
        c2 = Conv2D(32,
                    (3, 3),
                    activation='relu',
                    padding='same',
                    kernel_regularizer=self.regularizer)(down)

        # number of output layers
        outputs = []
        for _ in range(self.output_layers):
            out = Conv2D(2,
                         (1, 1),
                         activation='softmax',
                         padding='same',
                         kernel_regularizer=self.regularizer)(c2)
            outputs.append(out)

        # add more tasks together, if necessary
        if len(outputs) == 1:
            output_layer = outputs[0]
        else:
            output_layer = Concatenate(axis=3)(outputs)
        return input_layer, output_layer


    def _create_resudual_unet(self):

        input_layer = Input(shape=self.dim)
        n_maps = self.feature_maps
        f = [n_maps, n_maps*2, n_maps*4, n_maps*8, n_maps*16]


        ## Encoder
        e0 = input_layer
        e1 = stem(e0, f[0])
        e2 = residual_block(e1, f[1], strides=2)
        e3 = residual_block(e2, f[2], strides=2)
        e4 = residual_block(e3, f[3], strides=2)
        # e5 = residual_block(e4, f[4], strides=2)

        ## Bridge
        b0 = conv_block(e4, f[3], strides=1)
        b1 = conv_block(b0, f[3], strides=1)

        ## Decoder
        # u1 = upsample_concat_block(b1, e4)
        # d1 = residual_block(u1, f[4])

        u2 = upsample_concat_block(b1, e3)
        d2 = residual_block(u2, f[3])

        u3 = upsample_concat_block(d2, e2)
        d3 = residual_block(u3, f[2])

        u4 = upsample_concat_block(d3, e1)
        d4 = residual_block(u4, f[1])

        output_layer = Conv2D(2, (1, 1), padding="same", activation="softmax")(d4)
        # add more tasks together, if necessary
        # if len(outputs) == 1:
        #     output_layer = outputs[0]
        # else:
        #     output_layer = Concatenate(axis=3)(outputs)
        return input_layer, output_layer

    def load(self, model_path):
        # self.built = True
        # w, h, c = self.dim
        # self(np.zeros((1, w, h, c)))
        print(model_path)
        if os.path.isfile(model_path):
            self.model.load_weights(model_path)

    def set_model(self, dataset: Dataset, save_path):
        assert dataset is not None
        val_generator = dataset.get_validation_samples_generator()
        train_generator = dataset.get_training_samples_generator()

        score = {}

        for i in tqdm(range(min(len(train_generator), 42))):
            X, y = train_generator[i]

            y = y[..., 0] * 255
            result = self.model.predict(X)[..., -1] * 255

            for thr in range(1, 255):
                r = result > thr
                jcc = compute_jaccard(r, y)
                score[thr] = score.get(thr, 0) + jcc * r.shape[0]

        best_thr = max(score, key=score.get)
        print(best_thr)
        write_f_threshold(best_thr, '/'.join(save_path.split('/')[:-1]))

    def train_model(self, dataset: Dataset, save_path):
        val_generator = dataset.get_validation_samples_generator()
        train_generator = dataset.get_training_samples_generator()

        # defina callbacks
        reducelr_cb = ReduceLRCallback(
            factor=0.3,
            cooldown=5,
            patience=5,
            min_delta=1e-4,
            min_lr=0,
            verbose=0,
        )
        earlystop_cb = EarlyStopping(
            monitor="val_loss",
            min_delta=1e-4,
            patience=10,
            verbose=True,
            mode="auto",
            baseline=None,
        )
        model_checkpoint_cb = tf.keras.callbacks.ModelCheckpoint(
            save_path, monitor='val_loss',
            verbose=1,
            save_best_only=True,
            save_weights_only=True, 
            mode='auto',
            save_freq='epoch'
        )

        terminate_nan_cb = tf.keras.callbacks.TerminateOnNaN()
        tensorboard_cb = tf.keras.callbacks.TensorBoard(log_dir="./logs")
        callbacks = [reducelr_cb, earlystop_cb, terminate_nan_cb, model_checkpoint_cb]

        self.model.fit(train_generator,
                       steps_per_epoch=self.steps_per_epoch,
                       epochs=self.epochs,
                       callbacks=callbacks,
                       validation_data=val_generator,
                       use_multiprocessing=False,
                       workers=5)

    def predict_dataset(self, dataset: Dataset, batch_index=None):
        # with a final crop to original image size
        generator = dataset.get_img_generator()
        m, n = dataset.get_original_size()

        if batch_index is None:
            prediction = self.model.predict_generator(generator,
                                                      verbose=True,
                                                      use_multiprocessing=True,
                                                      workers=10)
        else:
            x = generator[batch_index]
            prediction = self.model.predict(x)

        prediction = prediction[:, :m, :n, :]
        return prediction

    def save_weights(self, path):
        self.model.save_weights(path)


def bn_act(x, act=True):
    x = BatchNormalization()(x)
    if act:
        x = Activation("relu")(x)
    return x


def conv_block(x, filters, kernel_size=(3, 3), padding="same", strides=1):
    conv = bn_act(x)
    conv = Conv2D(filters, kernel_size, padding=padding, strides=strides)(conv)
    return conv


def stem(x, filters, kernel_size=(3, 3), padding="same", strides=1):
    conv = Conv2D(filters, kernel_size, padding=padding, strides=strides)(x)
    conv = conv_block(conv, filters, kernel_size=kernel_size, padding=padding, strides=strides)

    shortcut = Conv2D(filters, kernel_size=(1, 1), padding=padding, strides=strides)(x)
    shortcut = bn_act(shortcut, act=False)

    output = Add()([conv, shortcut])
    return output


def residual_block(x, filters, kernel_size=(3, 3), padding="same", strides=1):
    res = conv_block(x, filters, kernel_size=kernel_size, padding=padding, strides=strides)
    res = conv_block(res, filters, kernel_size=kernel_size, padding=padding, strides=1)

    shortcut = Conv2D(filters, kernel_size=(1, 1), padding=padding, strides=strides)(x)
    shortcut = bn_act(shortcut, act=False)

    output = Add()([shortcut, res])
    return output


def upsample_concat_block(x, xskip):
    u = UpSampling2D((2, 2))(x)
    c = Concatenate()([u, xskip])
    return c
