import os
import argparse
import numpy as np
from .preprocessing import get_normal_fce, remove_uneven_illumination
import cv2
import glob
import requests
import zipfile
import logging
from datetime import datetime
from shutil import copyfile
import tensorflow as tf
from tensorflow.keras.regularizers import l1_l2
import re

import tifffile as tiff
from skimage.morphology import disk
from tqdm import tqdm
from time import sleep


class Normalizer:
    def __init__(self, normalization: str, uneven_illumination: bool = False):
        self.normalfce = get_normal_fce(normalization)
        self.uneven_illumination = uneven_illumination
        self.normalization = normalization

    def make(self, image):
        dim = image.shape
        # image = np.squeeze(image)
        if self.uneven_illumination:
            image = remove_uneven_illumination(image)
        image = self.normalfce(image)
        assert image.shape == dim, f'image changed shape from {dim} to {image.shape}'

        return image

    def get_normalfce(self):
        return self.normalfce


def get_image_shape(img_path, depth=1):
    # get one file
    if os.path.isdir(img_path):
        names = os.listdir(img_path)
        names = [name for name in names if name[-4:] in ['.png', '.tif']]
        assert len(names) > 0, f'There are no ".png" or ".tif" files in {img_path}'
        img_path = os.path.join(img_path, names[0])

    if os.path.isfile(img_path):
        img = cv2.imread(img_path, cv2.IMREAD_ANYDEPTH)
        assert img is not None, f'ER: cannot read image file: {img_path}'
        if len(img.shape) == 2:
            return img.shape[0], img.shape[1], depth
        return img.shape
    # assert False, img_path

    return None


def get_divisible_shape(original_size, divisor):
    mi, ni, depth = original_size
    new_mi = mi - mi % divisor
    if mi % divisor != 0:
        new_mi += divisor
    new_ni = ni - ni % divisor
    if ni % divisor != 0:
        new_ni += divisor
    return new_mi, new_ni


def get_formatted_shape(img_path, depth=1, divisor=16):
    """
    returns new and original image dimension
    new variables are declared to be divisible by 'divisor'

    :param img_path: path to an image directory, or image file path
    :param divisor: declared divisor of new dimensions
    :return: (new_n, new_m, n_channels), (original_m, original_n, n_channels)
    """

    original_size = get_image_shape(img_path, depth=depth)
    new_size = get_divisible_shape(original_size, divisor)

    return new_size, original_size


def load_flist(flist, prefix=''):
    """

    Args:
        flist:
        prefix: prefix of the expected file

    Returns: list of files

    """
    formats = ['.jpg', '.png', '.tif']
    if isinstance(flist, list):
        return flist

    # flist: image file path, image directory path, text file flist path
    if isinstance(flist, str):
        if os.path.isdir(flist):
            files = os.listdir(flist)
            files = [os.path.join(flist, f) for f in files if f[-4:] in formats and f.startswith(prefix)]
            files.sort()
            return files

        if os.path.isfile(flist):
            try:
                return np.genfromtxt(flist, dtype=np.str, encoding='utf-8')
            except:
                return [flist]

    return []


def clean_dir(dir_path):
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)
        return
    files = glob.glob(os.path.join(dir_path, '*'))
    for f in files:
        os.remove(f)


def download_pretrained_model(model_name):
    print(f'downloading pretrained model: {model_name} ...')
    url = f'https://www.fi.muni.cz/~xlux/deepwater/{model_name}.zip'

    # check if the url exists
    request = requests.get(url)
    if request.status_code != 200:
        print(f'Pretrained model {model_name} is not available.')
        return False

    zip_file = f'checkpoints/{model_name}.zip'

    myfile = requests.get(url)
    open(zip_file, 'wb').write(myfile.content)

    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(f'checkpoints')

    os.remove(zip_file)

    return True


def overlay_labels(o, labels):
    labels = np.squeeze(labels)
    o_rgb = cv2.cvtColor(o, cv2.COLOR_GRAY2RGB)
    labels_rgb = cv2.applyColorMap(labels.astype(np.uint8) * 15, cv2.COLORMAP_JET)

    fg_mask = (labels != 0).astype(np.uint8)
    bg_mask = (labels == 0).astype(np.uint8)

    labels_rgb[:, :, 0] = labels_rgb[:, :, 0] * fg_mask
    labels_rgb[:, :, 0] = labels_rgb[:, :, 0] + bg_mask * 180
    labels_rgb[:, :, 1] = labels_rgb[:, :, 1] + bg_mask * 180
    labels_rgb[:, :, 2] = labels_rgb[:, :, 2] + bg_mask * 180

    return cv2.addWeighted(o_rgb.astype(np.uint8), 0.7, labels_rgb, 0.3, 0)


def create_tracking(path, output_path, threshold=0.15):

    # check if path exists
    if not os.path.isdir(path):
        print('input path is not a valid path')
        return

    names = os.listdir(path)
    names = [name for name in names if '.tif' in name and 'mask' in name]
    names.sort()

    img = tiff.imread(os.path.join(path, names[0]))
    shape = img.shape

    print('Relabelling the segmentation masks.')
    records = {}

    old = np.zeros(img.shape)
    index = 1

    for i, name in enumerate(tqdm(names)):
        result = np.zeros(img.shape, np.uint16)

        img_path = os.path.join(path, name)
        img = tiff.imread(img_path)

        assert img.shape == shape, f'{img_path} -> {img.shape}'

        labels = np.unique(img)[1:]

        parent_cells = []

        for label in labels:
            mask = (img == label) * 1

            assert mask.shape == shape

            mask_size = np.sum(mask)
            overlap = mask * old
            candidates = np.unique(overlap)[1:]

            max_score = 0
            max_candidate = 0

            for candidate in candidates:
                score = np.sum(overlap == candidate * 1) / mask_size
                if score > max_score:
                    max_score = score
                    max_candidate = candidate

            if max_score < threshold:
                # no parent cell detected, create new track

                records[index] = [i, i, 0]
                result = result + mask * index
                index += 1
            else:

                if max_candidate not in parent_cells:
                    # prolonging track
                    records[max_candidate][1] = i
                    result = result + mask * max_candidate

                else:
                    # split operations
                    # if have not been done yet, modify original record
                    if records[max_candidate][1] == i:
                        records[max_candidate][1] = i - 1
                        # find mask with max_candidate label in the result and rewrite it to index
                        m_mask = (result == max_candidate) * 1
                        result = result - m_mask * max_candidate + m_mask * index

                        assert result.shape == shape

                        records[index] = [i, i, max_candidate.astype(np.uint16)]
                        index += 1

                    # create new record with parent cell max_candidate
                    records[index] = [i, i, max_candidate.astype(np.uint16)]
                    result = result + mask * index
                    index += 1

                # update of used parent cells
                parent_cells.append(max_candidate)
        # store result
        # print('result shape', result.shape)
        tiff.imwrite(os.path.join(output_path, name), result.astype(np.uint16))
        old = result

    # store tracking
    print('Generating the tracking file.')
    print('tracking results:', records)
    with open(os.path.join(output_path, 'res_track.txt'), "w") as file:
        for key in records.keys():
            file.write('{} {} {} {}\n'.format(key, records[key][0], records[key][1], records[key][2]))


def remove_edge_cells(label_img, border=20):
    if (border is None) or (border == 0):
        return label_img
    edge_indexes = get_edge_indexes(label_img, border=border)
    return remove_indexed_cells(label_img, edge_indexes)


def get_edge_indexes(label_img, border=20):
    mask = np.ones(label_img.shape)
    if len(mask.shape) == 2:
        mi, ni = mask.shape
        mask[border:mi - border, border:ni - border] = 0
    elif len(mask.shape) == 3:
        mi, ni, di = mask.shape
        mask[border:mi - border, border:ni - border, border:di - border] = 0
    else:
        assert False
    border_cells = mask * label_img
    indexes = np.delete(np.unique(border_cells), [0])

    result = []

    # get only cells with center inside the mask
    for index in indexes:
        cell_size = np.sum(label_img == index)
        gap_size = np.sum(border_cells == index)
        if cell_size * 0.5 < gap_size:
            result.append(index)

    return result


def remove_indexed_cells(label_img, indexes):
    mask = np.ones(label_img.shape)
    for i in indexes:
        mask -= (label_img == i)
    return label_img * mask


def safe_quantization(img, dtype=np.uint8):
    if dtype == np.uint8:
        img = np.maximum(img, 0)
        img = np.minimum(img, 255)

    return img.astype(dtype)


def find_sequences(path, input_prefix=''):
    dirs = os.listdir(path)
    if input_prefix == '':
        dirs = [d for d in dirs if len(d) == 2 and d.isdigit()]
    else:
        dirs = [d for d in dirs if input_prefix in d and len(d) == len(input_prefix) + 3 and d[:2].isdigit()]
    return dirs


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def generate_new_timestamp() -> str:
    """
    generates new :timestamp:
    Returns: unique string of timestamp
    """
    return datetime.now().strftime("%y%m%d-%H%M")


def create_base_directories(name, data_path, checkpoint_path):

    # check if there are data named as arg.name
    dataset_path = os.path.join(data_path, name)
    if not os.path.isdir(dataset_path):
        print(f"There is no dataset '{name}' in directory '{data_path}'")
        exit()

    if not os.path.exists(checkpoint_path):
        logging.info(f'Creating directory {checkpoint_path}')
        os.makedirs(checkpoint_path)
    checkpoint_path_name = os.path.join(checkpoint_path, name)
    if not os.path.exists(checkpoint_path_name):
        logging.info(f'Creating directory {checkpoint_path_name}')
        os.makedirs(checkpoint_path_name)


def get_config_path(name, data_path, experiment_path):
    # create config file in the experiment folder
    base_config_path = "config_example.yml"
    experiment_config_path = os.path.join(experiment_path, "config.yml")
    data_config_path = os.path.join(data_path, name, "config.yml")
    if not os.path.isfile(data_config_path):
        copyfile(base_config_path, data_config_path)
    if not os.path.isfile(experiment_config_path):
        copyfile(data_config_path, experiment_config_path)
    return experiment_config_path


def get_experiment_dir(args):
    name, path, exp_id = args.name, args.checkpoint_path, args.experiment
    mode = args.mode
    if exp_id is None and mode == 1:
        timestamp = generate_new_timestamp()
        args.experiment = timestamp
        experiment_dir = os.path.join(path, name, f"experiment_{timestamp}")
        os.mkdir(experiment_dir)
        print(f"Created experiment path: {experiment_dir}")
    elif exp_id is None:
        experiment_path = os.path.join(path, name)
        experiments = [path for path in os.listdir(experiment_path) if "experiment_" in path]

        print(f"ERROR: Pick the experiment id from the directory: {experiment_path}.")
        print(f'There are the following experiments: {experiments}')
        print("set parameter --experiment")
        # TODO: find the newest experiment and update the 'experiment id' automatically
        exit()
    else:
        experiment_dir = os.path.join(path, name, f"experiment_{exp_id}")
        if not os.path.isdir(experiment_dir):
            if mode == 1:
                experiment_path = os.path.join(path, name)
                if not os.path.isdir(experiment_path):
                    os.mkdir(experiment_path)
                os.mkdir(experiment_dir)
                print(f"INFO: Experiment {exp_id} was created.")
            else:
                print(f"ERROR: Experiment {exp_id} does not exist.")
                print(f"Check the directory {experiment_dir} or set the parameter --experiment properly")
                exit()
    return experiment_dir


def check_marker_configuration(config):
    """ marker parameters """
    if config.MARKER_ANNOTATIONS == 'weak':
        if not str(config.MARKER_DIAMETER).isdigit() or config.MARKER_DIAMETER == 0:
            print('set MARKER_DIAMETER in config file.')
            exit()
        config.SHRINK = 0
    elif config.MARKER_ANNOTATIONS == 'full':
        if not str(config.CELL_DIAMETER).isdigit() or config.CELL_DIAMETER == 0:
            print('set CELL_DIAMETER in config file.')
            exit()
        config.SHRINK = 70
        config.MARKER_DIAMETER = np.ceil((1 - (config.SHRINK / 100)) * config.CELL_DIAMETER).astype(np.uint8)

def correct_gt(gt):
    """
    corects augmented gt
        - relabel the image to have original labels for each object
        - remove approximated values on edges of masks
    :param gt:
    :return:
    """

    gt = cv2.medianBlur(gt, 5)
    new_gt = np.zeros(gt.shape, np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    kernel_er = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))

    objects = np.delete(np.unique(gt), [0])
    index = 1

    for i in objects:

        obj = cv2.inRange(gt, int(i), int(i))
        obj = cv2.morphologyEx(obj, cv2.MORPH_OPEN, kernel)
        ret, labels = cv2.connectedComponents(obj)

        if ret < 3:
            new_marker = labels.astype(np.uint8) * int(index)
            index += 1
        else:
            min_size = 50

            new_marker = np.zeros(gt.shape, np.uint8)

            for j in range(1, ret):

                single_obj = cv2.inRange(labels, int(j), int(j))
                j_size = np.sum(single_obj / 255)

                if j_size > min_size:
                    new_marker += single_obj // 255 * index
                    index += 1

        mask_marker = cv2.inRange(new_marker, 0, 0) // 255
        mask_gt = cv2.inRange(new_gt, 0, 0) // 255

        new_gt *= mask_marker
        new_gt += new_marker * mask_gt

    return new_gt


def get_regularizer(regularizer):
    if regularizer == 'l1l2':
        return l1_l2(l1=0.00001, l2=0.00001)
    else:
        return None


def get_flist_dic(img_files, input_depth=1):
    if input_depth > 1:
        return get_flist_dic_multi(img_files, input_depth)
    files_dict = {}
    for name in img_files:
        sample_id = get_image_id(name)
        assert not files_dict.get(sample_id)
        files_dict[sample_id] = [name]
    return files_dict


def get_flist_dic_multi(img_files, input_depth):
    files_dict = {}
    for name in img_files:
        sample_id = get_image_id(name)
        assert not files_dict.get(sample_id)

        id_digit = sample_id.split('_')[-1]
        assert id_digit.isdigit(), id_digit

        len_digit = len(id_digit)
        index = int(id_digit)
        indexes = [str(max(0, i)).zfill(len_digit) for i in
                   range(index - (input_depth//2), index + (input_depth//2) + 1)]
        assert len(indexes) == input_depth, indexes
        names = [name.replace(sample_id, sample_id[:-len_digit] + i) for i in indexes]
        max_index = 0
        # for max_index in range(input_depth):
        #     if not os.path.isfile(names[max_index]):
        #         break
        while max_index < input_depth and os.path.isfile(names[max_index]):
            max_index += 1
        # if max_index < input_depth - 1:
        for j in range(max_index, input_depth):
            names[j] = names[max_index-1]

        files_dict[sample_id] = names
    return files_dict


def count_digits(path):
    return len(get_image_id(path))


def get_image_id(path):
    name = os.path.split(path)[-1]
    ids = re.findall(r'\d+\_?\d+', name)
    assert len(ids) == 1
    img_id = ids[0]
    return img_id


def unfold_3D(path, markers=False, filter_empty=False):
    assert os.path.isdir(path)

    path_2d = f'{path}_2D'
    if not os.path.isdir(path_2d):
        os.mkdir(path_2d)

    assert os.path.isdir(path_2d)

    files = os.listdir(path)
    files = [f for f in files if '.tif' in f]
    files.sort()

    for file in tqdm(files):
        tiff_path = os.path.join(path, file)
        img_stack = tiff.imread(tiff_path)

        assert np.sum(img_stack) > 0

        for n_slice, img in enumerate(img_stack):
            slice_id = str(n_slice).zfill(3)
            file_name = file.replace('.tif', f'_{slice_id}.tif')
            file_path = os.path.join(path_2d, file_name)

            # if markers:
            #     img = markers_by_centroids(img)

            if not filter_empty or np.sum(img) > 0:
                cv2.imwrite(file_path, img)


def compute_jaccard(a, b):
    return (np.sum(np.logical_and(a, b)) + 1) / (np.sum(np.logical_or(a, b)) + 1)


def write_f_threshold(best_thr: int, save_path: str) -> None:
    config_path = os.path.join(save_path, 'config.yml')
    assert os.path.isfile(config_path), config_path

    with open(config_path, 'a+') as cf:
        cf.write(f'# automatic thr definition\nTHR_FOREGROUND: {best_thr}\n')



if __name__ == '__main__':
    create_tracking('/home/xlux/PROJECTS/deepwater/tmp/test_A549', '/home/xlux/PROJECTS/deepwater/tmp/test_A549')
