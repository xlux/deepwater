import tensorflow as tf
import math


def get_loss_function(name):
    if name == 'shapereg':
        return cewb_shape_regularized
    elif name == 'output2':
        return cross_entropy_weighted_balanced2
    elif name == 'ce_reg_entropy':
        return cross_entropy_reg_entropy
    return cross_entropy_weighted_balanced


def cross_entropy_weighted_balanced2(y_true, y_pred):
    ''' Cross Entropy weighted by a weight mask
    shape of y_true (b, w, h, 1)
    shape of y_pred (b, w, h, 2)
    y_true  - GT of weghted loss
            - 1st layer / segmentation mask, {0,1}
            - 2nd layer / pixel weights (1, 10)
    y_pred  - computed segmentation probabilities
            - 1st layer / background (0,1)
            - 2nd layer / foreground (0,1)
    '''

    epsilon: float = 1e-15

    foreground, pixel_weights = tf.split(y_true, 2, 3)
    prob_foreground, prob_background, _, _ = tf.split(y_pred, 4, 3)

    pixel_weights = tf.cast(pixel_weights, tf.float32)
    foreground = tf.cast(foreground, tf.float32)
    background = (1 - foreground)

    # assert pixel_weights.dtype == tf.float32, pixel_weights.dtype
    # assert foreground.dtype == tf.float32, foreground.dtype
    # assert background.dtype == tf.float32, background.dtype
    # assert prob_background.dtype == tf.float32, prob_background.dtype
    # assert prob_foreground.dtype == tf.float32, prob_foreground.dtype

    sum_foreground: float = tf.reduce_sum(foreground * pixel_weights) + epsilon
    sum_background: float = tf.reduce_sum(background * pixel_weights) + epsilon

    class_balance_mask = pixel_weights * (foreground / (2 * sum_foreground) +
                                          background / (2 * sum_background))

    loss = class_balance_mask * (background * tf.math.log(prob_background + epsilon) +
                                 foreground * tf.math.log(prob_foreground + epsilon))

    return - tf.reduce_sum(loss)


def cross_entropy_reg_entropy(y_true, y_pred):
    ''' Cross Entropy regularized by a entropy term
    shape of y_true (b, w, h, 2)
    shape of y_pred (b, w, h, 4)
    y_true  - GT of weighted loss
            - 1st layer / segmentation mask, {0,1}
            - 2nd layer / mask of unknown pixels, {0,1}
    y_pred  - computed segmentation probabilities
            - 1st layer / background probs (0,1)
            - 2nd layer / foreground probs (0,1)
            - 3rd layer / dummy
            - 4th layer / dummy
    '''

    epsilon: float = 1e-15

    foreground, pixel_weights = tf.split(y_true, 2, 3)
    prob_foreground, prob_background, _, _ = tf.split(y_pred, 4, 3)

    pixel_weights = tf.cast(pixel_weights, tf.float32)
    foreground = tf.cast(foreground, tf.float32)
    background = (1 - foreground)

    # assert pixel_weights.dtype == tf.float32, pixel_weights.dtype
    # assert foreground.dtype == tf.float32, foreground.dtype
    # assert background.dtype == tf.float32, background.dtype
    # assert prob_background.dtype == tf.float32, prob_background.dtype
    # assert prob_foreground.dtype == tf.float32, prob_foreground.dtype

    sum_foreground: float = tf.reduce_sum(foreground * pixel_weights) + epsilon
    sum_background: float = tf.reduce_sum(background * pixel_weights) + epsilon

    class_balance_mask = pixel_weights * (foreground / (2 * sum_foreground) +
                                          background / (2 * sum_background))

    loss = class_balance_mask * (background * tf.math.log(prob_background + epsilon) +
                                 foreground * tf.math.log(prob_foreground + epsilon))
    entropy = background * tf.math.log(prob_background)

    return - tf.reduce_sum(loss) - tf.reduce(entropy)


def cross_entropy_weighted_balanced(y_true, y_pred):
    ''' Cross Entropy weighted by a weight mask
    shape of y_true (b, w, h, 1)
    shape of y_pred (b, w, h, 2)
    y_true  - GT of weghted loss
            - 1st layer / segmentation mask, {0,1}
            - 2nd layer / pixel weights (1, 10)
    y_pred  - computed segmentation probabilities
            - 1st layer / background (0,1)
            - 2nd layer / foreground (0,1)
    '''

    epsilon: float = 1e-15

    foreground, pixel_weights = tf.split(y_true, 2, 3)
    prob_background, prob_foreground = tf.split(y_pred, 2, 3)

    pixel_weights = tf.cast(pixel_weights, tf.float32)
    foreground = tf.cast(foreground, tf.float32)
    background = (1 - foreground)

    # assert pixel_weights.dtype == tf.float32, pixel_weights.dtype
    # assert foreground.dtype == tf.float32, foreground.dtype
    # assert background.dtype == tf.float32, background.dtype
    # assert prob_background.dtype == tf.float32, prob_background.dtype
    # assert prob_foreground.dtype == tf.float32, prob_foreground.dtype

    sum_foreground: float = tf.reduce_sum(foreground * pixel_weights) + epsilon
    sum_background: float = tf.reduce_sum(background * pixel_weights) + epsilon

    class_balance_mask = pixel_weights * (foreground / (2 * sum_foreground) +
                                          background / (2 * sum_background))

    loss = class_balance_mask * (background * tf.math.log(prob_background + epsilon) +
                                 foreground * tf.math.log(prob_foreground + epsilon))

    return - tf.reduce_sum(loss)



def cewb_shape_regularized(y_true, y_pred):
    ''' Cross Entropy weighted by a weight mask
    shape of y_true (m, n, 1)
    shape of y_pred (m, n, 2)
    y_true  - GT of weghted loss
            - 1st layer / segmentation mask, {0,1}
            - 2nd layer / pixel weights (1, 10)
    y_pred  - computed segmentation probabilities
            - 1st layer / background (0,1)
            - 2nd layer / foreground (0,1)
    '''

    epsilon: float = 0.0000001
    alpha: float = 0.5

    foreground, pixel_weights = tf.split(y_true, 2, 3)
    prob_background, prob_foreground = tf.split(y_pred, 2, 3)

    background = (1 - foreground)

    sum_foreground: float = tf.reduce_sum(foreground * pixel_weights) + epsilon
    sum_background: float = tf.reduce_sum(background * pixel_weights) + epsilon

    class_balance_mask = pixel_weights * (foreground / (2 * sum_foreground) +
                                          background / (2 * sum_background))

    loss = class_balance_mask * (background * tf.math.log(prob_background + epsilon) +
                                 foreground * tf.math.log(prob_foreground + epsilon))

    shape_penalty = get_shape_reg(prob_foreground, mind=16, maxd=22)

    return - tf.reduce_sum(loss) * (1 - alpha) + shape_penalty * alpha


def get_first_image(img_stack):
    dim = tf.slice(tf.shape(img_stack), [1], [3])
    new_size = tf.concat([tf.constant([1]), dim], axis=0)
    result = tf.slice(img_stack, [0, 0, 0, 0], new_size, name=None)
    return result


def get_shape_reg(prob_map, maxd=16, mind=8):

    # instance
    instance = get_first_image(prob_map)

    # constants
    kernel = tf.constant(get_minmax_kernel(mind, maxd))
    cond = tf.less(tf.multiply(instance, 2), 1)
    shape = tf.shape(instance)

    c = tf.where(cond, tf.zeros(shape, dtype='int32'), tf.ones(shape, dtype='int32'))
    c = tf.concat([c, c], axis=3)
    c = tf.nn.erosion2d(c,
                        filters=kernel,
                        strides=[1, 1, 1, 1],
                        padding="SAME",
                        data_format="NHWC",
                        dilations=[1, 1, 1, 1]
                        )
    c = tf.nn.dilation2d(c,
                         filters=kernel,
                         strides=[1, 1, 1, 1],
                         padding="SAME",
                         data_format="NHWC",
                         dilations=[1, 1, 1, 1])

    ## minmax
    minmax = tf.reduce_sum(c, axis=[1, 2])
    supremum, infimum = tf.split(minmax, num_or_size_splits=2, axis=-1)

    supremum = tf.squeeze(supremum)
    infimum = tf.squeeze(infimum)

    pixel_sum = tf.cast(tf.reduce_sum(instance, axis=[1, 2]), 'int32')

    result = tf.reduce_mean(tf.divide(tf.subtract(supremum, infimum), pixel_sum))
    result = tf.cast(result, dtype='float32')

    return tf.subtract(tf.constant(1, dtype='float32'), result)


def tf_closing(c, size):
    c = -tf.nn.max_pool2d(input=-c, ksize=(size, size),
                          strides=1,
                          padding='SAME',
                          name='erosion2D')
    c = tf.nn.max_pool2d(c, ksize=(size, size),
                         strides=1,
                         padding='SAME',
                         name='dilation2D')
    return c


def get_shape_maxpool_reg(prob_map, maxd=24, mind=8):

    # constants
    cond = tf.less(tf.multiply(prob_map, 2), 1)
    shape = tf.shape(prob_map)

    c = tf.where(cond, tf.zeros(shape, dtype='int32'), tf.ones(shape, dtype='int32'))
    upper = tf_closing(c, maxd)
    lower = tf_closing(c, mind)

    suprem = tf.reduce_sum(lower, axis=[1, 2, 3])
    infim = tf.reduce_sum(upper, axis=[1, 2, 3])

    pixel_sum = tf.reduce_sum(prob_map, axis=[1, 2])
    pixel_sum = tf.cast(pixel_sum, 'int32')
    inter = tf.divide(tf.subtract(suprem, infim), pixel_sum)
    result = tf.reduce_mean(inter)
    result = tf.cast(result, dtype='float32')

    return tf.subtract(tf.constant(1, dtype='float32'), result)


def get_bincone(diameter, stride=1):
    size = diameter - 1
    distance_map = []
    for i in range(-size, size + 1):
        row = []
        for j in range(-size, size + 1):
            row.append(math.sqrt(abs(i) ** 2 + abs(j) ** 2))
        distance_map.append(row)
    result = [[[(k < s + .5) * 1 for s in range(0, size + 1, stride)]
               for k in row]
              for row in distance_map]

    return result


def get_minmax_kernel(radius_min, radius_max):
    size = radius_max
    distance_map = []
    for i in range(-size, size + 1):
        row = []
        for j in range(-size, size + 1):
            row.append(math.sqrt(abs(i) ** 2 + abs(j) ** 2))
        distance_map.append(row)
    result = [[[(k < s + .5) * 1 for s in [radius_min, radius_max]]
               for k in row]
              for row in distance_map]

    return result



def w_cren_2ch_bala_m(y_true, y_pred):
    ''' Cross Entropy weighted by a weight mask
    shape of y_true (m, n, 3)
    shape of y_pred (m, n, 4)
    y_true  - GT of weghted loss
            - 1st layer / segmentation mask, {0,1}
            - 2nd layer / weights <1, 4>
            - 3rd layer / borders {0,1}
    y_pred  - computed segmentation
            - 1st layer / segmentation (0,1)
            - 2nd layer / borders (0,1)
    '''

    LAMBDA = 0.0001

    m_true, w_true = tf.split(y_true, 2, 3)
    m0_prob, m1_prob = tf.split(y_pred, 2, 3)

    m_false = (1 - m_true)

    sum_Fm = tf.reduce_sum(m_true * w_true) + LAMBDA
    sum_Bm = tf.reduce_sum(m_false * w_true) + LAMBDA

    w_m_bala = w_true * m_true / (2 * sum_Fm) + w_true * m_false / (2 * sum_Bm)

    loss_m = w_m_bala * (m_false * tf.math.log(m0_prob + LAMBDA) + m_true * tf.math.log(m1_prob + LAMBDA))

    loss = - tf.reduce_mean(tf.reduce_sum(loss_m, axis=[1, 2, 3]), axis=0)

    return loss


def w_cren_2ch_bala(y_true, y_pred):
    ''' Cross Entropy weighted by a weight mask
    shape of y_true (m, n, 3)
    shape of y_pred (m, n, 4)
    y_true  - GT of weghted loss
            - 1st layer / segmentation mask, {0,1}
            - 2nd layer / weights <1, 4>
            - 3rd layer / borders {0,1}
    y_pred  - computed segmentation
            - 1st layer / segmentation (0,1)
            - 2nd layer / borders (0,1)
    '''

    LAMBDA = 0.0000001

    m_true, w = tf.split(y_true, 2, 3)
    m0_prob, m1_prob = tf.split(y_pred, 2, 3)

    m_false = (1 - m_true)

    sum_Fm = tf.reduce_sum(m_true * w) + LAMBDA
    sum_Bm = tf.reduce_sum(m_false * w) + LAMBDA

    w_m_bala = w * (m_true / (2 * sum_Fm) + m_false / (2 * sum_Bm))

    loss_m = w_m_bala * (m_false * tf.math.log(m0_prob + LAMBDA) + m_true * tf.math.log(m1_prob + LAMBDA))

    # loss = - tf.reduce_mean(tf.reduce_sum(loss_m, axis=[1, 2, 3]), axis=0)
    loss = - tf.reduce_sum(loss_m)

    return loss
