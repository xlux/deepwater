import pandas as pd
import os
from .utils import generate_new_timestamp
import csv


class Logger:

    COLUMNS = ["dataset", "experiment", "sequence", "model index", "timestamp",  "SEG", "DET"]
    INDEX = ["dataset", "experiment", "sequence", "model index"]

    def __init__(self, config):

        self.dataset = config.DATASET_NAME
        self.file_path = os.path.join(config.CONFIG_PATH, self.dataset, "results.csv")
        self.experiment = config.EXPERIMENT
        self.sequence = config.SEQUENCE
        if os.path.isfile(self.file_path):
            data = pd.read_csv(self.file_path,
                               dtype={'sequence': str, 'model index': str})
            data.set_index(Logger.INDEX, inplace=True)
            # print(data.index)
            # data.info()
        else:
            data = pd.DataFrame(columns=Logger.COLUMNS).set_index(Logger.INDEX)
        self.data = data

    def add(self, model, seg=None, det=None):
        timestamp = generate_new_timestamp()
        # define row
        row = pd.DataFrame([{"dataset": self.dataset,
                              "experiment": self.experiment,
                              "sequence": self.sequence,
                              "model index": model,
                              "timestamp": timestamp,
                              "SEG": seg,
                              "DET": det}]).set_index(["dataset", "experiment", "sequence", "model index"])
        row.to_csv("logs/test.csv")
        self._update_row(row)
        self._store_data()

    def _update_row(self, row):
        row_index = row.index[0]
        if row_index in self.data.index:
            self.data.update(row)
        else:
            self.data = self.data.append(row)
        self.data.sort_index()

    def _store_data(self):
        self.data.to_csv(self.file_path,
                         index=True,
                         quotechar='"',
                         quoting=csv.QUOTE_NONNUMERIC)
