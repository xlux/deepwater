import os
import numpy as np
import cv2
import subprocess
from tqdm import tqdm
from skimage.segmentation import watershed
import logging
from .logger import Logger
import tifffile
from .utils import get_image_shape
from shutil import copytree, rmtree
from tensorflow.keras import backend as K

from .dataset import Dataset
from .models import UNetModel
from .postprocessing import \
    postprocess_markers as pm,\
    postprocess_markers_09,\
    postprocess_foreground, \
    postprocess_markers_3D, \
    postprocess_foreground_3D
from .utils import \
    get_formatted_shape,\
    overlay_labels,\
    clean_dir,\
    create_tracking,\
    remove_edge_cells, \
    find_sequences, \
    get_image_id


class DeepWater:
    def __init__(self, config):
        self.config = config
        self.mode = config.MODE

        self.img_path = config.TEST_PATH
        self.viz_path = config.VIZ_PATH
        self.out_path = config.OUT_PATH
        self.res_path = config.RES_PATH

        self.name = config.DATASET_NAME
        self.model_name = config.MODEL_NAME
        self.seq = config.SEQUENCE
        self.batch_size = config.BATCH_SIZE
        self.version = config.VERSION
        self.timestamp = config.TIMESTAMP
        self.tracking = config.TRACKING
        self.display = config.DISPLAY_RESULTS
        self.debug = config.DEBUG
        self.dim = config.DIM
        self.config_path = config.CONFIGURATION_PATH
        self.new_model = config.NEW_MODEL
        self.m_model_name = config.MODEL_MARKER_NAME
        self.f_model_name = config.MODEL_FOREGROUND_NAME
        self.model_folder = config.EXPERIMENT_DIR
        self.input_depth = config.INPUT_DEPTH
        self.input_suffix = config.INPUT_SUFFIX
        self.track_threshold = config.TRACK_THRESHOLD
        self.compute_metrics = config.COMPUTE_METRICS

        self.marker_model = None
        self.foreground_model = None
        self.marker_dataset = None
        self.foreground_dataset = None

        self.regenerate_prediction = config.REGENERATE_PREDICTION
        # self.digits = self._get_n_digits()
        self.border = config.BORDER_MASK
        self.dataset = None
        self.ensemble_size = config.ENSEMBLE_SIZE

        self.train_markers = config.TRAIN_MARKERS
        self.train_foreground = config.TRAIN_FOREGROUND

        self.logfile = Logger(config)
        # print(self)

        print('DeepWater model was created!')

    # def load(self):
    #     self.marker_model = MarkerModel(self.config)
    #     self.foreground_model = MarkerModel(self.config)
    #     print('models were loaded')

    def test(self):

        assert os.path.isdir(self.img_path), self.img_path

        # create result path
        if not os.path.isdir(self.out_path):
            os.mkdir(self.out_path)
        clean_dir(self.out_path)

        # create display path
        # if self.display:
        #     if not os.path.isdir(self.viz_path):
        #         os.mkdir(self.viz_path)
        #     clean_dir(self.viz_path)

        self._set_img_shape()

        # create dataset instance
        self.dataset = Dataset(self.config)

        m_depth = self.config.MDEPTH if self.config.MDEPTH is not None else self.config.N_OUTPUTS
        f_depth = self.config.FDEPTH if self.config.FDEPTH is not None else self.config.N_OUTPUTS

        marker_model = UNetModel(self.config, out_depth=m_depth)
        foreground_model = UNetModel(self.config, out_depth=f_depth)


        """ test models """
        marker_models = [f for f in os.listdir(self.model_folder) if 'marker_model' in f]
        foreground_models = [f for f in os.listdir(self.model_folder) if 'foreground_model' in f]
        marker_models.sort()
        foreground_models.sort()

        if len(marker_models) != len(foreground_models):
            print(f'DEBUG: not equal numbers of models: \n foreground:{foreground_models} \n markers: {marker_models}')
            if len(marker_models) < len(foreground_models):
                marker_models = foreground_models
            else:
                foreground_models = marker_models

        idx_m = ["".join(c for c in m.replace(".h5", "") if c.isdigit())  for m in marker_models]
        idx_f = ["".join(c for c in f.replace(".h5", "") if c.isdigit()) for f in foreground_models]
        assert len(marker_models) == len(foreground_models)
        assert idx_m == idx_f, f"idx_m: {idx_m}\nidx_f: {idx_f}"

        if len(idx_m) == 0:
            print(f'There are no trained models detected in {self.model_folder}.')

        for index in idx_m:
            m_model_path = os.path.join(self.model_folder, f"marker_model_{index}.h5")
            f_model_path = os.path.join(self.model_folder, f"foreground_model_{index}.h5")

            marker_model.load(m_model_path)
            foreground_model.load(f_model_path)

            self._update_viz_path(index)
            self._predict_in_batches(marker_model, foreground_model)

            K.clear_session()

            print('Prediction completed!\n')
            if self.tracking:
                print('Creates tracking...\n')
                create_tracking(self.out_path, self.out_path, self.track_threshold)
            if self.display and ('3D' not in self.name):
                """ create new path for vizualization """
                self._store_visualisations()

            # if '3D' in self.name:
            #     self._merge_results()

            gt_path = os.path.join(self.config.DATA_PATH, self.name, f'{self.seq}_GT/SEG')
            if not os.path.isdir(gt_path):
                print(f'gt path {gt_path} does not exist.\nOnly image results were generated.')
            elif self.compute_metrics:
                seg, det = self.eval(eval_only=True, log=False)
                self._log_results(seg, det, index)

    def set_for_thr(self):
        """ control image folder """
        img_path = self.config.IMG_PATH
        assert os.path.isdir(img_path), img_path
        self._set_img_shape()

        """ initialize datasets """
        dataset_foreground = Dataset(self.config)
        model_path = os.path.join(self.model_folder, f"foreground_model_01.h5")
        tag = 'foreground'

        """ create model """
        model_f = UNetModel(self.config)

        """ load existing model """
        if os.path.isfile(model_path):
            print(f'loading {tag} model weights')
            model_f.load(model_path)

        print(f"training {tag} model")
        print(f"dataset length: {len(dataset_foreground)}")
        model_f.set_model(dataset_foreground, model_path)
        # model_f.save_weights(model_path)


    def _unlink_resdir(self):
        os.unlink(self.res_path)

    def _link_resdir(self):
        # remove existing files
        if os.path.isfile(self.res_path):
            print(f"INFO: file {self.res_path} exists. It has been removed.")
            os.unlink(self.res_path)
        elif os.path.isdir(self.res_path):
            print(os.listdir(self.res_path))
            print(f"INFO: directory {self.res_path} exists. It has been removed.")
            rmtree(self.res_path)

        # create symlink
        src = os.path.join(os.getcwd(), self.out_path)
        dist = os.path.join(os.getcwd(), self.res_path)
        assert not os.path.lexists(dist), f'{dist} exists; src: {src}'

        os.symlink(src, dist)
        assert os.path.lexists(dist), f'{dist} does not exist!; src: {src}'

    def _update_viz_path(self, index):
        display_folder = os.path.join(self.model_folder, f"VIZ_m{index}")
        if not os.path.isdir(display_folder):
            os.mkdir(display_folder)
        self.viz_path = os.path.join(display_folder, self.seq)
        if not os.path.isdir(self.viz_path):
            os.mkdir(self.viz_path)

    def _predict_in_batches(self, marker_model, foreground_model):
        n_batches = int(np.ceil(len(self.dataset) / self.batch_size))
        if self.debug > 2:
            self.dataset.save_all()

        m_depth = self.config.MDEPTH if self.config.MDEPTH is not None else self.config.N_OUTPUTS
        f_depth = self.config.FDEPTH if self.config.FDEPTH is not None else self.config.N_OUTPUTS

        # CTC BF datasets compatibility
        foreground_index = 1
        if self.version == 1.0:
            foreground_index = 3
        # CTC SIM+ datasets compatibility
        if self.version == 0.9:
            postprocess_markers = postprocess_markers_09
        else:
            postprocess_markers = pm

        batch_ids = self._get_batch_ids(n_batches)

        z_depth = self.dataset.get_z_depth()
        assert z_depth > 0

        m, n = self.dataset.get_original_size()
        
        marker_prediction = np.zeros((0, m, n))
        foreground_prediction = np.zeros((0, m, n))

        for batch_index in tqdm(range(n_batches)):

            ids = batch_ids[batch_index]

            # get new batch
            new_marker_pred = marker_model.predict_dataset(self.dataset, batch_index=batch_index)[..., -1]
            new_for_pred = foreground_model.predict_dataset(self.dataset, batch_index=batch_index)[..., -1]

            assert new_marker_pred.shape[1:] == (m, n)
            assert new_for_pred.shape[1:] == (m, n)

            # append to existing
            marker_prediction = np.concatenate([marker_prediction, new_marker_pred], axis=0)
            foreground_prediction = np.concatenate([foreground_prediction, new_for_pred], axis=0)

            tot_samples = marker_prediction.shape[0]
            n_imgs = tot_samples // z_depth

            for i in range(n_imgs):
                marker_image, marker_prediction = np.split(marker_prediction, [z_depth])
                foreground_image, foreground_prediction = np.split(foreground_prediction, [z_depth])

                assert foreground_image.shape == (z_depth, m, n), foreground_image.shape
                assert marker_image.shape == (z_depth, m, n), marker_image.shape

                marker_image = (marker_image * 255).astype(np.uint8)
                foreground_image = (foreground_image * 255 * 255).astype(np.uint16)

                if z_depth == 1:
                    marker_image = np.squeeze(marker_image)
                    foreground_image = np.squeeze(foreground_image)
                    _, marker_function = postprocess_markers(marker_image,
                                                             threshold=self.config.THR_MARKERS,
                                                             c=self.config.MARKER_DIAMETER,
                                                             h=self.config.MIN_MARKER_DYNAMICS,
                                                             dic=("DIC-C2DH-HeLa" in self.name))

                    foreground = postprocess_foreground(foreground_image,
                                                        threshold=self.config.THR_FOREGROUND)
                    # impose markers to foreground
                    foreground = np.maximum(foreground, (marker_function > 0) * 255)
                    segmentation_function = self._get_segmentation_function(foreground_image, foreground)

                    labels = watershed(segmentation_function, marker_function, mask=foreground)
                    labels = remove_edge_cells(labels, self.border)

                    self._store_results(ids[i], labels, marker_image, foreground_image, marker_function, foreground)
                else:
                    marker_function = postprocess_markers_3D(marker_image,
                                                             threshold=self.config.THR_MARKERS,
                                                             c=self.config.MARKER_DIAMETER)
                    foreground = postprocess_foreground_3D(foreground_image,
                                                           threshold=self.config.THR_FOREGROUND)

                    labels = watershed(-foreground_image, marker_function, mask=foreground)
                    # labels = remove_edge_cells(labels, self.border)

                    self._store_results_3D(ids[i], labels, marker_image, foreground_image, marker_function, foreground)

    def _get_batch_ids(self, n_batches):
        img_flist, _, _ = self._get_flists(self.dataset)
        # assert len(img_flist) == len(self.dataset), img_flist

        mi = self.input_depth // 2
        ids_dict = {}
        for bi in range(n_batches):
            batch_flist = img_flist[bi*self.batch_size:(1+bi)*self.batch_size ]
            batch_names = [stack[mi] for stack in batch_flist]

            batch_ids = [get_image_id(name) for name in batch_names]
            ids_dict[bi] = batch_ids
        return ids_dict

    def _get_ids(self):
        img_flist, _, _ = self._get_flists(self.dataset)
        assert len(img_flist) == len(self.dataset), f'{img_flist}, {self.dataset}'
        mi = self.input_depth // 2
        batch_names = [stack[mi] for stack in img_flist]
        batch_ids = [get_image_id(name) for name in batch_names]
        return batch_ids

    def _merge_results(self):
        all_files = os.listdir(self.out_path)
        all_files.sort()
        dict3d = {}

        for filename in all_files:
            if '.tif' not in filename:
                continue
            file_id = get_image_id(filename)
            # assert len(file_id) == self.digits
            sample_id, z_id = file_id.split('_')
            assert len(sample_id) == len(z_id), f'{file_id}, {filename}'
            sample_list = dict3d.get(sample_id, [])
            sample_list.append(filename)
            dict3d[sample_id] = sample_list

        for sample_id in dict3d.keys():
            files = dict3d[sample_id]
            files.sort()
            out_path = os.path.join(self.out_path, f'mask{sample_id}.tif')

            print(out_path)
            m, n, _ = get_image_shape(self.out_path)
            data = np.zeros((len(files), m, n), 'uint16')

            for i, filename in enumerate(files):
                file_path = os.path.join(self.out_path, filename)
                img = cv2.imread(file_path, cv2.IMREAD_ANYDEPTH)
                data[i, :, :] = img
                os.remove(file_path)

            print(data.shape, out_path)
            tifffile.imwrite(out_path, data, photometric='minisblack')

    def _store_results(self, index, labels, marker_image, foreground_image, marker_function, foreground):
        # store result
        cv2.imwrite(os.path.join(self.out_path, f'mask{index}.tif'),
                    labels.astype(np.uint16))

        if self.display:
            ws_functions = np.concatenate((marker_function, foreground), axis=1)
            cv2.imwrite(f'{self.viz_path}/ws_functions{index}.tif',
                        ws_functions.astype(np.uint8))

            cv2.imwrite(f'{self.viz_path}/m{index}.tif',
                        marker_image)
            cv2.imwrite(f'{self.viz_path}/c{index}.tif',
                        (foreground_image / 255).astype(np.uint8))

    def _store_results_3D(self, index, labels, marker_image, foreground_image, marker_function, foreground):
        # store result
        index = index.split('_')[0]
        tifffile.imwrite(os.path.join(self.out_path, f'mask{index}.tif'),
                         labels.astype(np.uint16),
                         photometric='minisblack')

        if self.display:
            ws_functions = np.concatenate(((marker_function>0)*255, foreground), axis=2)
            tifffile.imwrite(f'{self.viz_path}/ws_functions{index}.tif',
                             ws_functions.astype(np.uint8),
                             photometric='minisblack')

            tifffile.imwrite(f'{self.viz_path}/m{index}.tif',
                             marker_image,
                             photometric='minisblack')
            tifffile.imwrite(f'{self.viz_path}/c{index}.tif',
                             (foreground_image / 255).astype(np.uint8),
                             photometric='minisblack')


    def _get_segmentation_function(self, foreground_image, foreground):
        if self.version == 1.0:
            # imposing markers into segmenation function
            return np.maximum((255 - foreground_image), (255 - foreground))
        else:
            return -foreground_image

    def _store_visualisations(self):
        print("storing visualisations...\n")
        indexes = self._get_ids()
        m, n, _ = get_image_shape(self.img_path)

        assert len(indexes) == len(self.dataset.flist_img), f'{indexes}'

        for i in tqdm(range(len(self.dataset.flist_img))):
            o = self.dataset.get_image(i)
            o = ((o[:, :, self.input_depth//2] + .5) * 255).astype(np.uint8)
            o = o[:m, :n]
            # print(o.shape, dtype(o[0, 0]))
            index = indexes[i]
            result_path = f'{self.out_path}/mask{index}.tif'
            labels = cv2.imread(result_path, cv2.IMREAD_ANYDEPTH)
            labels = labels.astype(np.uint8)
            assert o.shape == labels.shape, f'{o.shape}, {labels.shape}'
            overlay = overlay_labels(o, labels)
            cv2.imwrite(f'{self.viz_path}/color_segmentation{index}.tif',
                        overlay.astype(np.uint8))
            # os.remove(result_path)

    def train(self):

        """ control image folder """
        img_path = self.config.IMG_PATH
        assert os.path.isdir(img_path), img_path
        self._set_img_shape()

        """ create model folder """
        if not os.path.isdir(self.model_folder):
            os.mkdir(self.model_folder)
            print(f'A model folder {self.model_folder} was created.')

        """ initialize datasets """
        """ train and store"""
        if self.train_markers:
            dataset_markers = Dataset(self.config, markers=True)
            for model_i in range(1, self.ensemble_size + 1):
                m_model_path = os.path.join(self.model_folder, f"marker_model_{model_i:02}.h5")
                self._train_model(m_model_path, dataset_markers, tag='markers')

        if self.train_foreground:
            dataset_foreground = Dataset(self.config)
            for model_i in range(1, self.ensemble_size + 1):
                f_model_path = os.path.join(self.model_folder, f"foreground_model_{model_i:02}.h5")
                self._train_model(f_model_path, dataset_foreground, tag='foreground')

    def eval(self, eval_only=False, log=True):
        gt_path = os.path.join(self.config.DATA_PATH, self.name, f'{self.seq}_GT/SEG')
        assert os.path.isdir(gt_path), f'GT path do not exists {gt_path}'

        if not eval_only and (self.regenerate_prediction or not self._verify_results()):
            print('Regenerating results...')
            self.test()
            assert self._verify_results(), 'Results are not consistent.'

        assert os.path.isdir(self.out_path), f'ERROR: {self.out_path} do not exists!\n' \
                                             f'cwd: {os.getcwd()}\n' \
                                             f':listdir {os.listdir(os.path.split(self.out_path)[0])}'

        names = [name for name in os.listdir(self.out_path) if '.tif' in name]
        assert len(names) > 0, f'There are no ".tif" files in {self.out_path}.'
        digits = len(get_image_id(names[0]))

        if digits > 4:
            digits //= 2

        self._link_resdir()
        dataset_path = os.path.join(self.config.DATA_PATH, self.name)
        cmd = ("./measures/SEGMeasure", dataset_path, self.seq, str(digits))
        seg_measure = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0].strip()
        seg = seg_measure.decode('utf8').split()

        cmd = ("./measures/DETMeasure", dataset_path, self.seq, str(digits))
        det_measure = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0].strip()
        det = det_measure.decode('utf8').split()
        self._unlink_resdir()

        cmd = ("./measures/TRAMeasure", dataset_path, self.seq, str(digits))
        tra_measure = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0].strip()
        tra = tra_measure.decode('utf8').split()
        self._unlink_resdir()

        try:
            seg = float(seg[-1])
            det = float(det[-1])
            print(f'Score of {dataset_path}, seq {self.seq}:')
            print(f'\t{seg}\n\t{det}')
        except ValueError:
            print('Evaluation ended with following errors. Result was not captured.')
            print(f'seg output: {seg}')
            print(f'det output: {det}')

        if log:
            self._log_results(seg, det, '01')
        return seg, det


    def _log_results(self, seg, det, index):
        logging.info(f"model {index} --- SEG: {seg}, DET: {det}")
        self.logfile.add(index, seg=seg, det=det)
        print(f"model {index} --- SEG: {seg}, DET: {det}")

    def _train_model(self, model_path, dataset, tag='un'):
        """ create model """
        model_f = UNetModel(self.config)

        """ load existing model """
        if os.path.isfile(model_path):
            print(f'loading {tag} model weights')
            model_f.load(model_path)

        print(f"training {tag} model")
        print(f"dataset length: {len(dataset)}")
        model_f.train_model(dataset, model_path)
        # model_f.save_weights(model_path)

    # sets atribut self.dim to the maximum
    def _set_img_shape(self):

        if self.config.MODE == 1:

            max_width, max_height = self.config.DIM

            seq = find_sequences(self.config.IMG_PATH)
            assert len(seq) > 0, f'there are no image sequences in {self.config.IMG_PATH}'
            # print(seq)

            for s in seq:
                if self.input_suffix:
                    s = f'{s}_{self.input_suffix}'
                    if not os.path.isdir(os.path.join(self.config.IMG_PATH, s)):
                        continue

                img_path = os.path.join(self.config.IMG_PATH, s)
                print(img_path)
                dim, _ = get_formatted_shape(img_path, depth=self.config.INPUT_DEPTH)
                width, height = dim
                max_width = max(max_width, width)
                max_height = max(max_height, height)
                print(s, max_width, max_height)
            self.config.DIM = max_width, max_height
        else:
            img_path = self.config.TEST_PATH
            dim, _ = get_formatted_shape(img_path, depth=self.config.INPUT_DEPTH)
            self.config.DIM = dim
        assert self.config.DIM != (0, 0), seq

    def _get_n_digits(self):
        path = self.config.IMG_PATH
        img_names = [name for name in os.listdir(path) if '.tif' in name]
        assert len(img_names) > 0, f'{img_names} {path}'
        img_id = get_image_id(img_names[0])
        return len(img_id)
        # return self.config.DIGITS

    def _verify_results(self):
        # gt_path = os.path.join(self.config.DATA_PATH, self.name, f'{self.seq}_GT/SEG')
        res_path = os.path.join(self.config.DATA_PATH, self.name, f'{self.seq}_RES')

        if not os.path.isdir(res_path):
            return False

        # TODO: test every gt file if it has proper mask
        return True

    def _get_model_path(self):
        """
        returns path to the folder with model files
        the name of the folder is "model_YYMMDD_HHMM"
        """
        path = os.path.join(self.config_path, self.model_name)
        files = [f for f in os.listdir(path) if "model_" in f and os.path.isdir(os.path.join(path, f))]
        files.sort()
        if self.mode == 1 and (len(files) == 0 or self.new_model):
            model_folder = os.path.join(path, f'model_{self.timestamp}')
            print(f"created new model folder: {model_folder}")
        else:
            print(self.mode, len(files) == 0 or self.new_model)
            assert len(files) != 0, "the folder contain no model files."
            model_folder = os.path.join(path, files[-1])
            print(f"loaded model folder: {model_folder}")
        return model_folder

    def _store_network_inputs(self, img_path, dataset, tag='marker'):
        debug_path = os.path.join(img_path, 'DEBUG')
        if not os.path.isdir(debug_path):
            os.mkdir(debug_path)
        print('test mode')
        print(f'Storing markers dataset to {debug_path}')
        images, gts = dataset.get_all()

        cwd = os.getcwd()

        for s in range(images.shape[0]):
            for d in range(images.shape[-1]):
                img = (images[s, ..., d] + .5) * 255
                cv2.imwrite(os.path.join(cwd, debug_path, f'{tag}_{s:03}_img_{d:03}.png'), img)
        for s in range(gts.shape[0]):
            for d in range(gts.shape[-1]):
                gt = gts[s, ..., d] * 255
                cv2.imwrite(os.path.join(cwd, debug_path, f'{tag}_{s:03}_gt_{d:03}.png'), gt)

    def _get_flists(self, dataset):
        return dataset._get_flists()
