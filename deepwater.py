import argparse
import logging
import os
from src.config import Config
from src.deepwater_object import DeepWater
from src.utils import str2bool,\
    create_base_directories, \
    get_config_path,\
    get_experiment_dir,\
    check_marker_configuration

# GLOBAL SETTING: turn off TensorFlow verbosity
if True:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def deepwater():
    config = load_config()

    # logging path definition
    logging.basicConfig(filename=config.log_file, level=logging.INFO)

    # load model
    model = DeepWater(config)

    # create log
    log_path = os.path.join(config.EXPERIMENT_DIR, 'experiment.log')
    logging.basicConfig(filename=log_path, level=logging.INFO)
    log = logging.getLogger(config.NAME)
    log.setLevel(logging.INFO)

    # model training
    if config.MODE == 1:
        # config.print()
        print('\nstart training...\n')
        model.train()

    # model test
    elif config.MODE == 2:
        print('\nstart segmentation...\n')
        model.test()

    # eval mode
    elif config.MODE == 3:
        print('\nstart eval...\n')
        model.eval(eval_only=True)

    elif config.MODE == 4:
        print('\nstart setting foreground threshold...\n')
        model.set_for_thr()

    else:
        print(f'\nERROR: incorrect mode {config.MODE}\n')


def load_config(mode=None):
    r"""loads model config

    Args:
        mode (int): 1: train, 2: test, 3: eval, reads from config file if not specified
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name',
                        type=str,
                        default=None,
                        help='pre-trained model name')
    parser.add_argument('--name',
                        type=str,
                        default=None,
                        required=True,
                        help='name of the dataset')
    parser.add_argument('--sequence',
                        type=str,
                        help='sequence number (01, 02, ...)')
    parser.add_argument('--checkpoint_path',
                        type=str,
                        default='checkpoints',
                        help='path to stored models (default: .checkpoints)')
    parser.add_argument('--data_path',
                        type=str,
                        default='datasets',
                        help='path to directory with datasets (default: .datasets)')
    parser.add_argument('--dw_path',
                        type=str,
                        default='.',
                        help='path to directory with deepwater')
    parser.add_argument('--mode',
                        type=int,
                        default=None,
                        choices=[1, 2, 3, 4],
                        help='mode of the program run')
    parser.add_argument('--validation_sequence',
                        type=str,
                        default=None,
                        help='sequence to validate the training. If it is not specified,'
                             'model is validated on randomly picked 10 percent  of training samples')
    parser.add_argument('--one_network',
                        type=str,
                        choices=['markers', 'foreground'],
                        default=None,
                        help='choose only one model to train')
    parser.add_argument('--new_model',
                        type=str2bool,
                        nargs='?',
                        default=False,
                        const=True,
                        help='choose to train new model from scratch')
    parser.add_argument('--annotations',
                        type=str,
                        choices=['full', 'weak'],
                        default=None,
                        help='markers from a full or a weak annotations')
    parser.add_argument('--test_settings',
                        type=str2bool,
                        nargs='?',
                        default=False,
                        const=True,
                        help='true to test an image pre-processing')
    parser.add_argument('--ensemble_size',
                        type=int,
                        default=1,
                        help='number of models trained as an ensemble')
    parser.add_argument('--experiment',
                        type=str,
                        default=None,
                        help='ID of the experiment to use, name of the directory is in a format "experiment_ID"')
    parser.add_argument('--model_id',
                        type=str,
                        default=None,
                        help='ID of the model to test or evaluate')
    parser.add_argument('--viz',
                        type=str2bool,
                        nargs='?',
                        default=False,
                        const=True,
                        help='display results')

    args = parser.parse_args()

    # create or find experiment dir
    create_base_directories(args.name, args.data_path, args.checkpoint_path)
    experiment_dir = get_experiment_dir(args)

    # load config file
    config_path = get_config_path(args.name, args.data_path, experiment_dir)
    print(f"the configuration file was loaded from {config_path}")
    config = Config(config_path)
    config.load_args(args)
    config.EXPERIMENT_DIR = experiment_dir

    # in a case that I want to use trained model for different data
    # it is possible to copy the model file directly
    config.MODEL_NAME = args.model_name if args.model_name is not None else args.name

    # for each mode we add different info to config
    # training mode
    if config.MODE == 1:

        """ path to the data """
        config.IMG_PATH = os.path.join(config.DATA_PATH, config.DATASET_NAME)
        config.VAL_SEQUENCE = args.validation_sequence
        config.NEW_MODEL = args.new_model

        if args.one_network is not None:
            if args.one_network == 'markers':
                config.TRAIN_FOREGROUND = False
            elif args.one_network == 'foreground':
                config.TRAIN_MARKERS = False

        check_marker_configuration(config)

    # testing mode
    # generates the results for a given sequence using the given model
    elif config.MODE == 2:
        config.IMG_PATH = os.path.join(config.DATA_PATH, config.DATASET_NAME, config.SEQUENCE)
        config.TEST_PATH = config.IMG_PATH  # merge variables

    # evaluation mode
    elif config.MODE == 3:
        pass

    elif config.MODE == 4:
        config.IMG_PATH = os.path.join(config.DATA_PATH, config.DATASET_NAME, config.SEQUENCE)
        config.TEST_PATH = config.IMG_PATH  # merge variables
        config.REFERENCE = 'GT'

    return config


if __name__ == "__main__":

    logging.info(f'Logging started')
    deepwater()
    logging.info('Logging finished')
